`timescale 100 ps/ 1 ps

// Design Name : sram
// File Name   : tb_sram.v
// Function    : testbech for sram.v
// Coder       : Babundin Anton


module tb_sram();

  reg enable;
  reg [3:0] address;
  reg [15:0] data;
  
  wire [15:0]  data_out;

  sram i1 (.address(address), .data(data), .data_out(data_out), .enable(enable));

  initial                                                
    begin 
      #1
      enable = 1'b1;
      address = 4'hF;
      data = 16'hFFFF;
        
      #5
      enable = 1'b0;
      data = 16'h0;
      address = 4'h0;
        
      #5
      address = 4'hF;
      
      #2
      $stop;                       
    end                                                    
     
endmodule 