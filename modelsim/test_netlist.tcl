
# vlog - вызов компилятора verilog для компиляции нашего кода непосредственно в ModelSim, "- work" - указываем библиотеку, "-O0" - отключаем всю оптимизацию, чтобы видеть все объекты (на схеме, в списках и т.п.) в том составе, как они описаны 
# То есть мы компилируем наш файл и добавляем его в библиотеку work 
vlog -work work -O0 "../modelsim/altera_primitives.v"
vlog -work work -O0 "../modelsim/cyclonev_atoms.v"
vlog -work work -O0 "../modelsim/cyclonev_atoms_ncrypt.v"
vlog -work work -O0 "../modelsim/altera_lnsim.sv"
vlog -work work -O0 "../modelsim/sram.vo"
   

# tb
vlog -work work -O0 "../tb/tb_sram.v"

# vsim - загружает новый дизайн в симулятор, далее путь к tb в ModelSim
#"-assertdebug" - подключаем утверждения для отладки ошибок($info, $error, $stop..)
# -L  Библиотека поиска для модулей, созданных на Verilog, указываеим библиотеку где лежат все наши модули
# "-msgmode" - команда переключает режимы отображения "сообщений"
# выводятся или в консоль или в графику или и туда и туда
# параметр ключа both значит и там и там
# -voptargs="+acc" для выборочной видимости объекта проектирования во время отладки
vsim work.tb_sram -assertdebug -L work -msgmode both -voptargs="+acc"   
 
# добавляем наши сигналы 
# "add" - команда добавить  что-либо 
# wave - добавляем указанный сигнал на временную диаграмму, далее указываем модуль и какой сигнал добавляем 
add wave /tb_sram/enable
add wave /tb_sram/data
add wave /tb_sram/address
add wave /tb_sram/data_out

# Шкала времени в пикосекундах
configure wave -timelineunits ps

# Запуск моделлирования
# -all - выполнение моделлирования до конца (в нашем случае до $stop)
run -all
