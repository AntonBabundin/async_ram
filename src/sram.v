// Design Name : sram
// File Name   : sram.v
// Function    : SRAM 256 x 16
// Coder       : Babundin Anton

module sram
(
  input wire          enable,
  input wire [15:0]     data,
  input wire [3:0]   address,
  
  output wire  [15:0] data_out
);

  reg [15:0] memory [15:0];  //Declare RAM
  reg [3:0] addr_reg;

  always @*
    begin
      if(enable)
        begin
          memory[address] <= data;
        end
    end
    
  always @*
    begin
      addr_reg <= address;
    end  
    
  assign data_out = memory[addr_reg];
    
endmodule 